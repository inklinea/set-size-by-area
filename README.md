##############################################################################
# Set Size By Area, add a scale transform to an existing path to set area
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Modify Path>Set Size By Area
##############################################################################

You can test the results using Extensions>Visualise Path>Measure Path