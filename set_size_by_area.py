#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Set Size By Area, add a scale transform to an existing path to set area
# An Inkscape 1.2.1+ extension
# Appears under Extensions>Modify Path>Set Size By Area
##############################################################################

import math

import inkex
from inkex import Transform
from inkex.bezier import csparea

conversions = {
    'in': 96.0,
    'pt': 1.3333333333333333,
    'px': 1.0,
    'mm': 3.779527559055118,
    'cm': 37.79527559055118,
    'm': 3779.527559055118,
    'km': 3779527.559055118,
    'Q': 0.94488188976378,
    'pc': 16.0,
    'yd': 3456.0,
    'ft': 1152.0,
    '': 1.0,  # Default px
}

def set_path_area(self, path_object, target_area):

    csp = path_object.path.transform(path_object.composed_transform()).to_superpath()

    path_area = abs(csparea(csp))

    # Lets get the fraction that path area is of target area.

    scale_factor = (target_area * (SetSizeByArea.cf**2)) / path_area

    tranform_scale = math.sqrt(scale_factor)

    # inkex.errormsg(f'Path Area {path_area} -- Target Area {target_area} -- Area Fraction {scale_factor} Area Fraction SQ {tranform_scale}')

    transform_scale = Transform().add_scale(tranform_scale)

    path_object.transform = path_object.transform @ transform_scale


class SetSizeByArea(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--target_area_float", type=float, dest="target_area_float", default=500)
        pars.add_argument("--unit_choice_combo", type=str, dest="unit_choice_combo", default='px')
    
    def effect(self):

        unit_choice = self.options.unit_choice_combo

        SetSizeByArea.cf = conversions[unit_choice] / conversions[self.svg.unit]

        selected_paths = self.svg.selection.filter(inkex.PathElement)

        if len(selected_paths) < 1:
            inkex.errormsg('No Paths Found')
            return

        for path_object in selected_paths:

            set_path_area(self, path_object, self.options.target_area_float)
        
if __name__ == '__main__':
    SetSizeByArea().run()
